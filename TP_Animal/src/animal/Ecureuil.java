package animal;

public class Ecureuil implements Animal {
	
	private int noisette;
	private int pointDeVie;
	
	public Ecureuil() {
		this.noisette = 0;
		this.pointDeVie = 5;
	}
	
	public Ecureuil(int pv) {
		if(pv <= 0) {
			this.pointDeVie = 0;
		}else {
			this.pointDeVie = pv;
		}
	}
	
	public void stockerNourriture(int nourriture) {
		if(nourriture > 0) {
			this.noisette += nourriture;
		}
	}
	
	public boolean etreMort() {
		if(this.pointDeVie == 0) {
			return true;
		}
		return false;
	}
	
	public boolean passerUnjour() {
		if(this.etreMort()) {
			return false;
		}
		if(this.noisette == 0) {
			if(this.pointDeVie < 3) {
				this.pointDeVie = 0;
				return false;
			}else {
				this.pointDeVie -= 2;
				return true;
			}
		}else {
			this.noisette--;
			if(this.noisette > 0) {
				this.noisette--;
			}
			return true;
		}
	}
	
	public int getPv() {
		return this.pointDeVie;
	}
	
	public int getStockNourriture() {
		return this.noisette;
	}
	
	public String toString() {
		return "Ecureuil - pv:" + this.pointDeVie + " noisettes:" + this.noisette;
	}
}
