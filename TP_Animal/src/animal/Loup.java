/**
 * 
 */
package animal;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Loup implements Animal {

	/**
	 * Nombre de viande stock� pouvant �tre positif ou nul
	 */
	private int viande;

	/**
	 * Nombre de point de vie pouvant �tre positif ou nul
	 */
	private int pointDeVie;

	/**
	 * Constructeur vide de Loup.java avec son nombre de point de vie � 30 et sa
	 * viande � 0
	 */
	public Loup() {
		this.pointDeVie = 30;
		this.viande = 0;
	}

	/**
	 * Constructeur de Loup.java pour un nombre de point de vie donn� et sa viande
	 * initialis�e � 0
	 * 
	 * @param pointDeVie Nombre de point de vie (Vallant 0 si le param�tre est
	 *                   n�gatif
	 */
	public Loup(int pointDeVie) {
		if (pointDeVie < 0)
			pointDeVie = 0;

		this.pointDeVie = pointDeVie;
		this.viande = 0;
	}

	@Override
	public boolean etreMort() {
		return this.getPv() <= 0;
	}

	@Override
	public int getPv() {
		return this.pointDeVie;
	}

	@Override
	public int getStockNourriture() {
		return this.viande;
	}

	@Override
	public boolean passerUnjour() {
		if (!this.etreMort()) {
			if (!(this.getStockNourriture() == 0)) {
				this.viande -= 1;
				this.viande = Math.floorDiv(this.viande, 2);
				return true;
			} else {
				if(this.getPv() < 4)
					this.pointDeVie = 0;
				else
					this.pointDeVie -= 4;
				
				return !this.etreMort();
			}
		} else {
			return false;
		}
	}

	@Override
	public void stockerNourriture(int nourriture) {
		if(nourriture < 0)
			nourriture = 0;
		
		this.viande += nourriture;
	}

	@Override
	public String toString() {
		return "Loup - pv:" + this.getPv() + " viande:" + this.getStockNourriture();
	}

}
