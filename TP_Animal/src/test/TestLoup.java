package test;

import static org.junit.Assert.*;

import org.junit.Test;

import animal.Animal;
import animal.Loup;

public class TestLoup {

	@Test
	public void testConstructeurVide() {
		Animal loup = new Loup();
		assertEquals("Les points de vie du Loup devrait être égal à 30", 30, loup.getPv());
		assertEquals("Le loup ne devrait pas posseder de nourriture", 0, loup.getStockNourriture());
	}
	
	@Test
	public void TestConstructeurNormal() {
		Animal loup = new Loup(8);
		assertEquals("Les points de vie du Loup devrait être égal à 8", 8, loup.getPv());
		assertEquals("Le loup ne devrait pas posseder de nourriture", 0, loup.getStockNourriture());
	}

	@Test
	public void TestConstructeurPVnegatif() {
		Animal loup = new Loup(-8);
		assertEquals("Les points de vie du Loup devrait être nuls", 0, loup.getPv());
		assertEquals("Le  loup ne devrait pas posseder de nourriture", 0, loup.getStockNourriture());
	}
	
	@Test
	public void TestStocherNourriturePositif() {
		Animal loup = new Loup();
		loup.stockerNourriture(5);
		assertEquals("Le  loup devrait posseder 5 viandes", 5, loup.getStockNourriture());
	}
	
	@Test
	public void TestStocherNourritureNegatif() {
		Animal loup = new Loup();
		loup.stockerNourriture(5);
		loup.stockerNourriture(-3);
		assertEquals("Le  loup ne devrait pas perdre de viande", 5, loup.getStockNourriture());
	}
	
	@Test
	public void TestPasserUnjourNormalAvecViandePerimeeNormal() {
		Animal loup = new Loup();
		loup.stockerNourriture(9);
		boolean vivant = loup.passerUnjour();
		assertEquals("Le  loup devrait passé la nuit", true, vivant);
		assertEquals("Le  loup devrait possèder plus que 4 viandes", 4, loup.getStockNourriture());
	}
	
	@Test
	public void TestPasserUnjourNormalAvecViandePerimeeArrondi() {
		Animal loup = new Loup();
		loup.stockerNourriture(8);
		boolean vivant = loup.passerUnjour();
		assertEquals("Le  loup devrait passé la nuit", true, vivant);
		assertEquals("Le  loup devrait possèder plus que 3 viandes", 3, loup.getStockNourriture());
	}
	
	@Test
	public void TestPasserUnjourPerdPV() {
		Animal loup = new Loup();

		boolean vivant = loup.passerUnjour();
		
		assertEquals("Le  loup devrait passé la nuit", true, vivant);
		assertEquals("Le  loup ne devrait avoir plus que 26 pv ", 26, loup.getPv());
	}
	
	@Test
	public void TestPasserUnjourPassePasLaNuit() {
		Animal loup = new Loup(4);
		boolean vivant = loup.passerUnjour();
		assertEquals("Le  loup ne devrait pas passé la nuit", false, vivant);
		assertEquals("Le  loup ne devrait plus avoir de pv ", 0, loup.getPv());
		
		loup = new Loup(2);
		vivant = loup.passerUnjour();
		assertEquals("Le  loup ne devrait pas passé la nuit", false, vivant);
		assertEquals("Le  loup ne devrait plus avoir de pv ", 0, loup.getPv());
	}
	
	@Test
	public void TestPasserUnjourDejaMort() {
		Animal loup = new Loup(0);
		boolean vivant = loup.passerUnjour();
		assertEquals("Le  loup ne devrait pas passé la nuit, il etait deja mort", false, vivant);
		assertEquals("Le  loup ne devrait pas avoir de pv, il etait deja mort", 0, loup.getPv());
	}
	
	@Test
	public void TestToString() {
		Animal loup = new Loup();
		loup.stockerNourriture(5);
		String s = loup.toString();
		assertEquals("L'affichage n'est pas valide", "Loup - pv:30 viande:5", s);
		
	}
}
