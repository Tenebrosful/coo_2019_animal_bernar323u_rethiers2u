/**
 * Test de la classe Ecureuil.java
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import animal.Animal;
import animal.Ecureuil;

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class TestEcureuil {

	/**
	 * Teste la cr�ation normale d'un Ecureuil sans param�tre
	 */
	@Test
	public void constructeurVide() {
		Animal e = new Ecureuil();

		assertEquals("Le nombre de point de vie devrait �tre � 5", 5, e.getPv());
		assertEquals("Le nombre de noisette devrait �tre 0", 0, e.getStockNourriture());
	}

	/**
	 * Teste la cr�ation normale d'un Ecureuil avec 3 pv
	 */
	@Test
	public void constructeurPvValide() {
		Animal e = new Ecureuil(3);

		assertEquals("Le nombre de point de vie devrait �tre � 3", 3, e.getPv());
		assertEquals("Le nombre de noisette devrait �tre 0", 0, e.getStockNourriture());
	}

	/**
	 * Teste la cr�ation normal d'un Ecureuil avec -2 pv
	 */
	@Test
	public void constructeurPvNegatif() {
		Animal e = new Ecureuil(-2);

		assertEquals("Le nombre de point de vie devrait �tre � 0", 0, e.getPv());
		assertEquals("Le nombre de noisette devrait �tre 0", 0, e.getStockNourriture());
	}

	/**
	 * Teste la m�thode stockerNourriture
	 */
	@Test
	public void stockerNourritureValide() {
		Animal e = new Ecureuil();
		e.stockerNourriture(3);

		assertEquals("Le nombre de nourriture devrait �tre � 3", 3, e.getStockNourriture());
	}

	/**
	 * Teste la m�thode stockerNourriture avec un param�tre n�gatif
	 */
	@Test
	public void stockerNourritureNegatif() {
		Animal e = new Ecureuil();
		e.stockerNourriture(-3);

		assertEquals("Le nombre de nourriture devrait �tre � 0", 0, e.getStockNourriture());
	}

	/**
	 * Teste la m�thode �tre mort
	 */
	@Test
	public void etreMort() {
		Animal e1 = new Ecureuil();
		Animal e2 = new Ecureuil(0);

		assertEquals("La fonction devrait retourner false", false, e1.etreMort());
		assertEquals("La fonction devrait retourner true", true, e2.etreMort());
	}

	/**
	 * Teste la m�thode passerUnjour lorsqu'il est en vie et a assez de nourriture
	 */
	@Test
	public void passerUnjourNormal() {
		Animal e = new Ecureuil();

		e.stockerNourriture(10);

		assertEquals("La fonction devrait retourner true", true, e.passerUnjour());
		assertEquals("L'�cureuil devrait avoir 8 noisettes", 8, e.getStockNourriture());
	}

	/**
	 * Teste la m�thode passerUnjour lorsque l'�cureuil est d�j� mort
	 */
	@Test
	public void passerUnjourDejaMort() {
		Animal e = new Ecureuil(0);

		assertEquals("La fonction devrait retourner false", false, e.passerUnjour());
	}

	/**
	 * Teste la m�thode passerUnjour lorsque l'�cureuil n'a pas de nourriture et
	 * doit perdre de la vie
	 */
	@Test
	public void passerUnjourPertePv() {
		Animal e = new Ecureuil();

		e.passerUnjour();

		assertEquals("L'�cureuil devrait avoir 3pv", 3, e.getPv());
	}

	/**
	 * Test la m�thode passerUnjour lorsque l'�cureuil n'a qu'une seule noisette
	 */
	@Test
	public void passerUnjourUneNourriture() {
		Animal e = new Ecureuil();
		
		e.stockerNourriture(1);
		e.passerUnjour();
		
		assertEquals("L'�cureuil devrait avoir 0 nourriture",0,e.getStockNourriture());
	}
	
	/**
	 * Teste la m�thode passerUnjour avec un �cureuil sans nourriture devant mourir
	 */
	@Test
	public void passerUnjourMort() {
		Animal e = new Ecureuil(2);
		
		assertEquals("La fonction devrait retourner false", false, e.passerUnjour());
	}
	
	/**
	 * Teste la m�thode toString
	 */
	@Test
	public void ToString() {
		Ecureuil e = new Ecureuil();
		
		String res = "Ecureuil - pv:5 noisettes:0";
		assertEquals("La fonction devrait retourner '" + res + "'", res, e.toString());
	}
}
